import logging

from redis import StrictRedis

# config.py 文件负责项目不同阶段的配置


# 创建自定义配置类
class Config(object):
    # 开启debug模式
    DEBUG = True
    
    # 配置mysql数据库
    SQLALCHEMY_DATABASE_URI = "mysql://root:mysql@127.0.0.1:3306/information24"
    # 关闭数据库跟踪
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    
    # 配置redis数据库
    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = 6379
    
    # session数据转储到redis数据库的配置
    SECRET_KEY = 'sasdf47490)_@*@_$*JFWWUJFKSfsrvbsdgzgaju98UDS-yNN*Y_ R MHNAY*)#()OJ'
    SESSION_TYPE = 'redis'
    # 具体保存到那个数据库, 创建redis数据库对象
    SESSION_REDIS = StrictRedis(host=REDIS_HOST, port=REDIS_PORT)
    # 开启加密
    SESSION_USE_SIGNER = True
    # 不需要永久存储
    SESSION_PERMANENT = False
    # 设置session的失效时间
    PERMANENT_SESSION_LIFETIME = 86400
    
    
class DevelopmentConfig(Config):
    # 开发模式的配置信息
    DEBUG = True
    # 设置开发的日志级别
    LOG_LEVEL = logging.DEBUG


class ProductionConfig(Config):
    # 生产环境的配置信息
    DEBUG = False
    # 设置生产环境的日志级别
    LOG_LEVEL = logging.WARNING
    

# 给外界提供一个调用接口
config_dict = {
    "development": DevelopmentConfig,
    "production": ProductionConfig
}

