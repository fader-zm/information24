# 数据库迁移
from flask_migrate import Migrate, MigrateCommand
# 导入管理类，使用命令行启动项目
from flask_script import Manager
# 为mysqldb自动切换python2环境
import pymysql
pymysql.install_as_MySQLdb()
from info import create_app, db, redis_store
from flask import current_app
import logging
from info import models

# from config import config_dict

"""
代码抽取：
    单一职责原则， 一个py文件做一件事情， 各司其职， 方便项目后期维护， 降低项目耦合度
"""

# manage.py 文件负责项目的启动

# 创建app对象
app = create_app("development")

# 给项目添加数据库迁移能力
Migrate(app, db)

# 创建管理类对象
manager = Manager(app)

# 使用管理对象添加迁移命令
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    # app.run()
    # 使用管理类对象运行项目
    print(app.url_map)
    manager.run()
