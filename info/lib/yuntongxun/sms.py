# -*- coding:utf-8 -*-
from info.lib.yuntongxun.CCPRestSDK import REST
import ssl
ssl._create_default_https_context = ssl._create_unverified_context


# --------------------------------需要改成自己账号的内容------------------------------------
# 说明：主账号，登陆云通讯网站后，可在"控制台-应用"中看到开发者主账号ACCOUNT SID
_accountSid = '8a216da86904c0600169536fbd8d24f7'

# 说明：主账号Token，登陆云通讯网站后，可在控制台-应用中看到开发者主账号AUTH TOKEN
_accountToken = '26a9783f7bf04555ab8ff5eaddb5ae6b'

# 请使用管理控制台首页的APPID或自己创建应用的APPID
_appId = '8a216da86904c0600169536fbdea24fe'
# --------------------------------需要改成自己账号的内容------------------------------------



# 说明：请求地址，生产环境配置成app.cloopen.com
_serverIP = 'app.cloopen.com'

# 说明：请求端口 ，生产环境为8883
_serverPort = "8883"

# 说明：REST API版本号保持不变
_softVersion = '2013-12-26'

# 云通讯官方提供的发送短信代码实例
# # 发送模板短信
# # @param to 手机号码
# # @param datas 内容数据 格式为数组 例如：{'12','34'}，如不需替换请填 ''
# # @param $tempId 模板Id
#
# def sendTemplateSMS(to, datas, tempId):
#     # 初始化REST SDK
#
#     # 每次发送短信验证码之前都进行用户权限认证这是耗时的网络操作，只需验证`一次`
#     rest = REST(serverIP, serverPort, softVersion)
#     rest.setAccount(accountSid, accountToken)
#     rest.setAppId(appId)
#
#     result = rest.sendTemplateSMS(to, datas, tempId)
#     for k, v in result.iteritems():
#
#         if k == 'templateSMS':
#             for k, s in v.iteritems():
#                 print '%s:%s' % (k, s)
#         else:
#             print '%s:%s' % (k, v)

# c1 = CCP()
# c2 = CCP()
# c3 = CCP()


class CCP(object):
    """发送短信的辅助类"""

    def __new__(cls, *args, **kwargs):
        # 判断是否存在类属性_instance，_instance是类CCP的唯一对象，即单例
        # 当对象还未创建的时候，第一次调用，CCP类中并没有_instance属性
        if not hasattr(CCP, "_instance"):

            #  将客户端和云通信的权限鉴定操作封装到单列中提高性能
            # 创建_instance对象，让CCP类保存
            cls._instance = super(CCP, cls).__new__(cls, *args, **kwargs)
            # 每次发送短信验证码之前都进行用户权限认证这是耗时的网络操作，只需验证`一次`将他封装到单列模式中
            # 好处： 提高代码性能
            # 用户权限校验
            cls._instance.rest = REST(_serverIP, _serverPort, _softVersion)
            cls._instance.rest.setAccount(_accountSid, _accountToken)
            cls._instance.rest.setAppId(_appId)

        # 直接返回CCP中的_instance属性
        return cls._instance

    def send_template_sms(self, to, datas, temp_id):
        """发送模板短信"""
        # @param to 手机号码
        # @param datas 内容数据 格式为数组 例如：{'125665','5'}，如不需替换请填 ''
        # @param temp_id 模板Id
        result = self.rest.sendTemplateSMS(to, datas, temp_id)

        print(result)
        # 如果云通讯发送短信成功，返回的字典数据result中statuCode字段的值为"000000"
        if result.get("statusCode") == "000000":
            # 返回0 表示发送短信成功
            return 0
        else:
            # 返回-1 表示发送失败
            return -1


if __name__ == '__main__':
    ccp = CCP()
    # 注意： 测试的短信模板编号为1
    ccp.send_template_sms('18791920372', ['1234', 5], 1)
    
