# 自定义过滤器, 显示新闻排行的前三位样式
from flask import session, jsonify, g, current_app


from info.response_code import RET


def do_rank_calss(rank):
    if rank == 1:
        return "first"
    elif rank == 2:
        return "second"
    elif rank == 3:
        return "third"
    else:
        return ""
    

# 自定义装饰器, 获取用户登录对象

def get_user_info(view_func):
    """用户登录验证"""
    def wrapper(*args, **kwargs):
        # 1.user_id : 用户id
        user_id = session.get("user_id")
        # 会出现db循环导入, 延迟导入
        from info.models import User
        # 2.查询数据库获取用户对象
        user = None
        if user_id:
            try:
                user = User.query.get(user_id)
            except Exception as e:
                current_app.logger.error('数据库数据查询异常', e)
                return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")
        # 3.返回用户对象
        g.user = user
        # 原有视图函数再次调用
        result = view_func(*args, **kwargs)
        return result
    return wrapper




