# 将app的创建封装到工厂方法中
import logging
from email import generator
from logging.handlers import RotatingFileHandler
from flask import Flask, g, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import generate_csrf
from redis import StrictRedis
from flask_wtf import CSRFProtect
# 调整flask.session存储位置的工具类
from flask_session import Session
from config import config_dict
from info.utils.common import do_rank_calss, get_user_info

# 当app对象为空的情况下， 数据库并没有进行真正的初始化操作
db = SQLAlchemy()

# 创建redis_store对象
redis_store = None  # type: StrictRedis


def write_log(config_class):
    # 设置日志的记录等级
    logging.basicConfig(level=config_class.LOG_LEVEL)  # 调试debug级
    # 创建日志记录器，指明日志保存的路径、每个日志文件的最大大小、保存的日志文件个数上限
    file_log_handler = RotatingFileHandler("logs/log", maxBytes=1024 * 1024 * 100, backupCount=10)
    # 创建日志记录的格式 日志等级 输入日志信息的文件名 行数 日志信息
    formatter = logging.Formatter('%(levelname)s %(filename)s:%(lineno)d %(message)s')
    # 为刚创建的日志记录器设置日志记录格式
    file_log_handler.setFormatter(formatter)
    # 为全局的日志工具对象（flask app使用的）添加日志记录器
    logging.getLogger().addHandler(file_log_handler)


def create_app(config_name):
    # 创建项目对象
    app = Flask(__name__)
    
    # 获取项目配置类
    config_class = config_dict[config_name]
    
    # 加载配置
    app.config.from_object(config_class)
    
    # 记录日志
    write_log(config_class)
    
    # 创建mysql数据库对象
    db = SQLAlchemy(app)
    # 懒加载， 延迟初始化
    # 当app存在的情况下，才真实的进行数据的初始化操作
    db.init_app(app)
    
    # 懒加载
    global redis_store
    # 创建redis数据库对象
    redis_store = StrictRedis(host=config_class.REDIS_HOST, port=config_class.REDIS_PORT, decode_responses=True)
    
    # 给项目配置csrf保护机制
    CSRFProtect(app)
    
    # 每次请求之后随机生成csrf_token,随cookie发送给浏览器
    @app.after_request
    def set_csrt_token(response):
        # 1. 生成csrf_token随机值
        csrf_token = generate_csrf()
        # 2. 借助响应对象设置到cookie中
        response.set_cookie("csrf_token", csrf_token)
        # 3. 返回响应对象
        return response

    # 捕捉404异常
    @app.errorhandler(404)
    @get_user_info
    def page_not_found(err):
        user = g.user
        data = {
            "user_info": user.to_dict() if user else None
        }
        return render_template('news/404.html', data=data)
    
    # 将session数据保存到redis数据库中
    Session(app)
    
    # 延迟导入解决循环导入问题
    from info.moduls.index import index_bp
    # 将index_db对象注册到app中
    app.register_blueprint(index_bp)
    # 将passport_bp对象注册到app中
    from info.moduls.passport import passport_bp
    app.register_blueprint(passport_bp)
    # 将news_bp对象注册到app中
    from info.moduls.news import news_bp
    app.register_blueprint(news_bp)
    
    # 添加自定义过滤器
    app.add_template_filter(do_rank_calss, "rank_class")
    
    
    
    return app



