# 导入蓝图对象
from flask import request, jsonify, current_app, render_template, session, g, abort

from info import constants
from info.models import News, User
from info.response_code import RET
from info.utils.common import get_user_info
from . import news_bp

"""
创建新闻详情模板, 返回新闻详情模板
"""


# /news/123
@news_bp.route('/<string:news_id>')
@get_user_info
def news_detail(news_id):
    """
    新闻详情展示
    """
    # ===============1. 用户信息展示===========
    """
    判断用户是否登录
    1. 获取参数
        1.1 使用session获取当前登录用户的user_id
    2. 参数校检
        2.1 判空
    3. 获取当前登录的用户对象
        3.1 user = User.query.get(user_id)
        3.2 将对象列表转化为字典列表
    4. 组织参数
        将user对象加到data中
    """
    
    # # 1.1 使用session获取当前登录用户的user_id
    # user_id = session.get('user_id', '')
    # # 2.1 判空
    # user = None  # type: User
    # user_dict = []
    # if user_id:
    #     # 获取当前登录的用户对象
    #     try:
    #         user = User.query.get(user_id)
    #     except Exception as e:
    #         current_app.logger.error('数据库数据查询异常', e)
    #         return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")
    user = g.user
    user_dict = []
    # 3.2 将对象列表转化为字典列表
    user_dict = user.to_dict() if user else []
    # 将user对象加到data中
    
    # =================2. 点击排行显示=============
    news_rank = None
    try:
        news_rank = News.query.order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.logger.error('数据库数据查询异常', e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")
    news_rank_list = []
    for news in news_rank if news_rank else []:
        news_rank_list.append(news.to_dict())
        
    # =================3. 新闻详情页展示 ============
    """
    1. 获取参数
    2. 参数校检
        2.1 判空news_id
    3. 业务逻辑
        3.1 根据news_id获取新闻对象
        3.2 将对象列表转化成字典列表
        3.3 组织data数据
    4. 返回数据
        4.1 渲染新闻详情页面, 返回data数据
    """
    # 1. 获取参数
    # 2.1 判空news_id
    if not news_bp:
        return jsonify(errno=RET.PARAMERR, errmsg='参数不足')
    # 3.1 根据news_id获取新闻对象
    news = None
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询新闻对象异常")
    # 3.2 将对象列表转化成字典列表
    if not news:
        abort(404)
    news_dict_list = []  # type: News
    news_dict_list = news.to_dict() if news else []
    # 3.3 组织data数据
    data = {
        "news": news_dict_list,
        "user_info": user_dict,
        "news_clicks_list": news_rank_list,
    }
    # 4.1 渲染新闻详情页面, 返回data数据
    return render_template('news/detail.html', data=data)










































