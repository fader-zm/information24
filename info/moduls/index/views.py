from info import constants
from info.models import User, News, Category
from info.response_code import RET
from info.utils.common import get_user_info
from . import index_bp
# from info import redis_store
from flask import render_template, current_app, session, jsonify, request, g, abort


# from flask import current_app


# 3. 使用蓝图对象装饰视图函数
# 查询首页新闻列表数据
@index_bp.route('/news_list')
def get_news_list():
    """查询首页新闻列表数据"""
    """
    1. 获取参数
        1.1新闻分类(category)的id: cid  第几页: page, 默认为1  每页显示多少数据: per_page 默认:comstants.HOME_PAGE_MAX_NEWS
    2. 参数校检
        2.1 判空 cid
        2.2 将数据进行int强制类型转换
    3. 业务逻辑
      3.1 cid作为查询条件获取查询对象, 再用paginate方法进行数据分页处理
      3.2 调用分页对象的属性 获取当前页所有数据, 当前页码, 总页码
      3.3 将对象列表转化成字典列表
    4. 返回数据
    """
    # 1.1新闻分类(category)的id: cid  第几页: page, 默认为1  每页显示多少数据: per_page 默认:comstants.HOME_PAGE_MAX_NEWS
    param_dict = request.args
    cid = param_dict.get('cid')
    page = param_dict.get('page', 1)
    per_apge = param_dict.get('per_page', constants.HOME_PAGE_MAX_NEWS)
    # 2.1 判空 cid
    if not cid:
        return jsonify(errno=RET.PARAMERR, errmsg='参数不足')
    
    # 2.2 将数据进行int强制类型转换
    try:
        page = int(page)
        per_apge = int(per_apge)
        cid = int(cid)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="参数错误")
    
    # 3.1 cid作为查询条件获取查询对象, 再用paginate方法进行数据分页处理
    # 判断cid是否为1, 若为1,将所有新闻按照创建时间排序, 返回最新数据
    # 定义一个过滤条件列表
    filter_list = []
    if cid != 1:
        filter_list.append(News.category_id == cid)
    
    # 返回分页对象  参数3: 关闭错误输出, 自己捕获
    news_list = []
    current_page = 1
    total_page = 1
    
    try:
        paginate = News.query.filter(*filter_list).order_by(News.update_time.desc()).paginate(page, per_apge, False)
        
        # 3.2 调用分页对象的属性 获取当前页所有数据, 当前页码, 总页码
        # 当前页的所有数据
        news_list = paginate.items
        # 当前页码
        current_page = paginate.page
        # 新闻分类的总页数
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error('数据库数据查询异常:', e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")
    
    # 3.3 将对象列表转化成字典列表
    # 定义一个新闻字典列表
    news_dict_list = []
    for news in news_list if news_list else []:
        news_dict_list.append(news.to_dict())
    
    data = {
        "newsList": news_dict_list,
        "current_page": current_page,
        "total_page": total_page
    }
    return jsonify(errno=RET.OK, errmsg='查询新闻列表数据成功', data=data)


# 新闻首页展示
@index_bp.route('/')
@get_user_info
def index():
    # 新闻首页
    # ------------------1. 展示用户基本信息-----------------
    # # 1. 获取当前登录用户的id
    # user_id = session.get('user_id', '')
    # user_dict = None
    # # 2. 判断user_id是否存在
    # if user_id:
    #     # 若存在,在mysql数据库中查询用户信息
    #     try:
    #         # 根据user_id查询用户对象
    #         user = User.query.get(user_id)
    #     except Exception as e:
    #         current_app.logger.error(e)
    #         # current_app.logger.error(e)
    #         return jsonify(errno=RET.DBERR, errmsg="查询mysql数据库中用户信息异常")
    #     # 将对象列表转换成字典列表
    user = g.user
    user_dict = user.to_dict() if user else None
    
    # ---------------------2. 查询新闻列表数据---------------
    # 1. 查询News表, 按最新发布时间降序排列, 取前6位数据
    try:
        news_data = News.query.order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DATAERR, errsmg="查询新闻排行数据库异常")
    news_clicks_list = list()
    for news in news_data if news_data else []:
        # 将对象列表转换成字典列表
        news_clicks_list.append(news.to_dict())
    
    # ---------------------3.新闻分类列表数据 ---------------
    category_list = list()
    # 查询所有分类数据
    try:
        news_category = Category.query.filter().all()
    except Exception as e:
        current_app.logger.error('数据库数据查询异常', e)
        return jsonify(errno=RET.DBERR, errmsg='数据库数据查询异常')
    # 将对象列表转换成字典列表
    for category in news_category if news_category else []:
        category_list.append(category.to_dict())
    
    # 3. 返回用户到浏览器
    data = {
        "user_info": user_dict,
        "news_clicks_list": news_clicks_list,
        "categories": category_list
    }
    return render_template("news/index.html", data=data)


@index_bp.route('/favicon.ico')
def favicon():
    return current_app.send_static_file('news/favicon.ico')
