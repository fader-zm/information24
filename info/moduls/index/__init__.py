from flask import Blueprint

"""
1. 导入蓝图类
2. 创建蓝图对象
3. 使用蓝图对象装饰视图函数
4. 将蓝图对象添加到app中
"""

index_bp = Blueprint("index", __name__)

from .views import *




