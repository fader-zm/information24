"""
1. 导入蓝图类
2. 创建蓝图对象
3. 使用蓝图对象装饰视图函数
4. 在app项目对象中注册蓝图对象
"""

# 1. 导入蓝图类
from flask import Blueprint

# 2. 创建蓝图对象, url_prefix 访问前缀
passport_bp = Blueprint("passport_db", __name__, url_prefix='/passport')

from .views import *


